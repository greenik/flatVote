Template.unvotedFlats.helpers({
    unvotedFlats: function () {
        return Flats.find({voted: {$nin: [Session.get('loginName')]}});
    }
});
