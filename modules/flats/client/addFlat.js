var addFlatOlx = function(flatURL) {
    var htmlString, el, title, image, desc, price;
    Meteor.call( 'openSession', flatURL, function( err, res ) {
        if(err) {
            console.log(err);
            sAlert.error('<div class="text-center"><h4>Coś się spierdoliło :(</h4></div>');
        } else {
            var prefix = 'http://';
            //dodawanie http://
            if (flatURL.substr(0, prefix.length) !== prefix)
            {
                flatURL = prefix + flatURL;
            }
            //ucinanie z linku wszystkiego po hashu
            if(flatURL.indexOf('#') > 0) {
                flatURL = flatURL.substring(0, flatURL.indexOf('#'));
            }
            //pobranie html z linku do elementu DOM
            htmlString = res.content;
            el = document.createElement( 'html' );
            el.innerHTML = htmlString;
            //Sprawdzanie czy ogłoszenie jest aktualne
            var errorElement = el.getElementsByClassName('lheight20 large cfff');
            console.log(errorElement.length);
            // var errorNode = errorElement.childNodes[0];
            // var errorVal = errorNode.nodeValue;
            if(errorElement.length) {
                sAlert.error('<div class="text-center"><h4>Ogłoszenie nieaktualne</h4></div>');
            } else {
                //wyciaganie tytulu ogloszenia
                var titleElement = el.getElementsByTagName('h1')[0];
                var titleNode = titleElement.childNodes[0];
                var titleVal = titleNode.nodeValue;
                title = titleVal.trim();
                //obrazek ogloszenia
                var imageElement = el.getElementsByClassName('block br5 {nr:1}');
                if(imageElement[0].pathname == '/add'){
                    image = 'http://www.arapneumatik.pl/uploads/images/no-picture.gif';
                } else {
                    image = imageElement[0].href;
                }
                //opis ogloszenia
                var descElement = el.getElementsByClassName('pding10 lheight20 large')[0];
                var descNode = descElement.childNodes[0];
                var descVal = descNode.nodeValue;
                desc = descVal.trim();
                //cena ogloszenia
                var priceElement = el.getElementsByClassName('xxxx-large')[0];
                var priceNode = priceElement.childNodes[0];
                var priceVal = priceNode.nodeValue;
                price = priceVal.trim();
                //budowanie obiektu do dodania do bazy
                var flatObj = {
                    URL: flatURL,
                    title: title,
                    image: image,
                    desc: desc,
                    price: price,
                    score: 0,
                    date: new Date()
                };
                Meteor.call( 'addFlat', flatObj, function(e, r) {
                    if(e) {
                        console.log(e);
                        sAlert.error('<div class="text-center"><h4>Coś się spierdoliło :(</h4></div>');
                    } else {
                        if(r) {
                            var text;
                            if(r.numberAffected && r.insertedId) {
                                text = '<div class="text-center"><h4>Mieszkanko dodane!</h4><a href="/"><small>Idź do listy</small></a></div>';
                            } else if(r.numberAffected && !r.insertedId) {
                                text = '<div class="text-center"><h4>Mieszkanko zaktualizowane!</h4><a href="/"><small>Idź do listy</small></a></div>';
                            }
                            sAlert.success(text);
                            var resetInput = document.getElementById('flatURL').value ="";
                        }
                    }
                });
            }
        }
    });
};

var addFlatGumtree = function(flatURL) {
    var htmlString, el, title, image, desc, price;
    Meteor.call( 'openSession', flatURL, function( err, res ) {
        if(err) {
            console.log(err);
            sAlert.error('<div class="text-center"><h4>Coś się spierdoliło :(</h4></div>');
        } else {
            var prefix = 'http://';
            //dodawanie http://
            if (flatURL.substr(0, prefix.length) !== prefix)
            {
                flatURL = prefix + flatURL;
            }
            //pobranie html z linku do elementu DOM
            htmlString = res.content;
            el = document.createElement( 'html' );
            el.innerHTML = htmlString;

            //wyciaganie tytulu ogloszenia
            var titleElement = el.getElementsByClassName('myAdTitle')[0];
            var titleNode = titleElement.childNodes[0];
            var titleVal = titleNode.nodeValue;
            title = titleVal.trim();

            //obrazek ogloszenia
            var imageElement = el.getElementsByTagName('img');
            if(imageElement[1].pathname == '/add'){
                image = 'http://www.arapneumatik.pl/uploads/images/no-picture.gif';
            } else {
                image = imageElement[1].src;
            }

            //opis ogloszenia
            var descElement = el.getElementsByClassName('pre')[0];
            var descNode = descElement.childNodes[0];
            var descVal = descNode.nodeValue;
            desc = descVal.trim();

            //cena ogloszenia
            var priceElement = el.getElementsByClassName('amount')[0];
            var priceNode = priceElement.childNodes[0];
            var priceVal = priceNode.nodeValue;
            price = priceVal.trim();

            //budowanie obiektu do dodania do bazy
            var flatObj = {
                URL: flatURL,
                title: title,
                image: image,
                desc: desc,
                price: price,
                score: 0,
                date: new Date()
            };

            console.log(flatObj);

            Meteor.call( 'addFlat', flatObj, function(e, r) {
                if(e) {
                    console.log(e);
                    sAlert.error('<div class="text-center"><h4>Coś się spierdoliło :(</h4></div>');
                } else {
                    if(r) {
                        var text;
                        if(r.numberAffected && r.insertedId) {
                            text = '<div class="text-center"><h4>Mieszkanko dodane!</h4><a href="/"><small>Idź do listy</small></a></div>';
                        } else if(r.numberAffected && !r.insertedId) {
                            text = '<div class="text-center"><h4>Mieszkanko zaktualizowane!</h4><a href="/"><small>Idź do listy</small></a></div>';
                        }
                        sAlert.success(text);
                        var resetInput = document.getElementById('flatURL').value ="";
                    }
                }
            });
        }
    });
};

Template.addFlat.events({
    'submit form': function(event){
        event.preventDefault();
        var flatURL = event.target.flatURL.value;
        var htmlString, el, title, image, desc, price;
        var url = new URL(flatURL);

        if(url.host.indexOf('olx') >= 0) {
            addFlatOlx(flatURL);
        } else if(url.host.indexOf('gumtree') >= 0) {
            addFlatGumtree(flatURL);
        } else {
            sAlert.error('<div class="text-center"><h4>Nieprawidłowy link :(</h4></div>');
        }
    }
});
