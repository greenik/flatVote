Future = Npm.require('fibers/future');

function isValidURL(url)
{
    try {
        var uri = new URI(url);
        // URI has a scheme and a host
        return (!!uri.scheme() && !!uri.host());
    }
    catch (e) {
        // Malformed URI
        return false;
    }
}

Meteor.methods({
    openSession: function(url) {
        var fut = new Future();
        Meteor.http.get(url, function( err, res ){
            fut.return(res);
        });

        // if( (url.indexOf('.html') < 1) || (url.indexOf('olx.pl') < 0) || (url.indexOf('gumtree.pl') < 0)) {
        //     throw new Meteor.Error(500, 'Error!');
        // }

        // Force method to wait on Future return
        return fut.wait();
    },
    addFlat: function(obj) {
        return Flats.upsert({
            URL: obj.URL
        }, {
            $set: obj
        });
    }

});
