Template.home.onCreated(function() {
    // console.log("HOME");
});

Template.home.helpers({
    flats: function () {
        return Flats.find({}, {sort: {score: -1}});
    }
});

Template.home.events({

});

Template.flatItem.helpers({
    deleteButton: function() {
        if(Session.get('iAmAdmin')) {
            return '<a><span class="text-danger delete-flat">&times;</span></a>';
        }
    },
    voted: function() {
        var thisFlat = Flats.findOne(this._id);
        if(!thisFlat || !thisFlat.voted) {
            return false;
        } else {
            return (thisFlat.voted.indexOf(Session.get('loginName')) > -1);
        }
    }
});

Template.flatItem.events({
    'submit form': function(e, t){
        e.preventDefault();
        var flatComment = e.target.flatComment.value;
        if(flatComment.length > 0) {
            Flats.update({_id: this._id}, {$push: {comments: {author: Session.get('loginName'), content: flatComment}}});
            sAlert.success('<div class="text-center"><h4>Komentarz dodany!</h4></div>');
            var resetInput = document.getElementById('flatComment').value ="";
        } else {
            sAlert.error('<div class="text-center"><h4>Komentarz nie może być pusty</h4></div>');
        }
    },
    'click .score-up': function (e, t) {
        var thisFlat = Flats.findOne(this._id);
        // console.log(thisFlat);
        if (!thisFlat.voted || (thisFlat.voted.indexOf(Session.get('loginName')) < 0)) {
            Flats.update({
                _id: this._id
            }, {
                $inc: {score: 1},
                $push: {
                    'voted': Session.get('loginName'),
                    'unvote': {name: Session.get('loginName'), value: -1}
                }
            });
        }
    },
    'click .score-down': function (e, t) {
        var thisFlat = Flats.findOne(this._id);
        // console.log(thisFlat);
        if (!thisFlat.voted || (thisFlat.voted.indexOf(Session.get('loginName')) < 0)) {
            Flats.update({
                _id: this._id
            }, {
                $inc: {score: -1},
                $push: {
                    'voted': Session.get('loginName'),
                    'unvote': {name: Session.get('loginName'), value: 1}
                }
            });
        }
    },
    'click .delete-flat': function (e, t) {
        if(confirm('Na pewno chcesz usunąć ogłoszenie?')) {
            Flats.remove({_id: this._id});
        }
    },
    'click .delete-vote': function (e, t) {
        var thisFlat = Flats.findOne(this._id);
        // console.log(thisFlat)
        var unvoteValue = thisFlat.unvote.filter(function( obj ) {
          return obj.name == Session.get('loginName');
        });
        var votedObj = _.without(thisFlat.voted, Session.get('loginName'));
        // console.log(votedObj);
        var unvoteObj = _.without(thisFlat.unvote, unvoteValue[0]);
        // console.log(unvoteObj);
        Flats.update({
            _id: thisFlat._id
        }, {
            $inc: {score: unvoteValue[0].value},
            $set: {
                'voted': votedObj,
                'unvote': unvoteObj
            }
        });
    }
});
