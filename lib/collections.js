Flats = new Meteor.Collection('Flats');

this.Pages = new Meteor.Pagination(Flats, {
  itemTemplate: "flatItem",
  perPage: 3,
  sort: {
    date: 1
  }
});