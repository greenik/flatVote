Router.configure({
    layoutTemplate: 'layout'
});

Router.route('home', {
    name: 'home',
    path: '/',
    action: function () {
        if (this.ready) {
            this.render();
        }
    }
});

Router.route('addFlat', {
    name: 'addFlat',
    path: '/add',
    action: function () {
        if (this.ready) {
            this.render();
        }
    }
});

Router.route('bestFlats', {
    name: 'bestFlats',
    path: '/best',
    action: function () {
        if (this.ready) {
            this.render();
        }
    }
});

Router.route('unvotedFlats', {
    name: 'unvotedFlats',
    path: '/unvoted',
    action: function () {
        if (this.ready) {
            this.render();
        }
    }
});
