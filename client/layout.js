Meteor.startup(function () {
    sAlert.config({
        effect: 'jelly',
        position: 'bottom',
        timeout: 5000,
        html: true,
        onRouteClose: true,
        stack: true,
        offset: 0, // in px - will be added to first alert (bottom or top - depends of the position in config)
        beep: false
        // examples:
        // beep: '/beep.mp3'  // or you can pass an object:
        // beep: {
        //     info: '/beep-info.mp3',
        //     error: '/beep-error.mp3',
        //     success: '/beep-success.mp3',
        //     warning: '/beep-warning.mp3'
        // }
    });

});

Template.layout.helpers({
    loginName: function () {
        return Session.get('loginName');
    },
    inactiveDelete: function() {
        if(Session.get('iAmAdmin')) {
            return '<li><a href="#" class="delete-archived">Usuń nieaktualne</a></li>';
        }
    },
});

Template.layout.events({
    'click .choose-user': function (e, t) {
        // console.log(e.target.dataset.user);
        Session.set('loginName', e.target.dataset.user);
    },
    'click .delete-archived': function (e, t) {
        var flats = Flats.find({}, {fields: {URL: 1}}).fetch();
        var urls = _.pluck(flats, 'URL');
        urls.forEach(function (element) {
            Meteor.call( 'openSession', element, function( err, res ) {
                if(err) {
                    console.log(err);
                } else {
                    //pobranie html z linku do elementu DOM
                    htmlString = res.content;
                    el = document.createElement( 'html' );
                    el.innerHTML = htmlString;
                    //wyciaganie tytulu ogloszenia
                    var inactiveElement = el.getElementsByClassName('lheight20 large cfff');
                    // console.log(inactiveElement);
                    if(inactiveElement.length > 0) {
                        var flatId = Flats.findOne({URL: element})._id;
                        Flats.remove({_id: flatId});
                        sAlert.success('<div class="text-center"><h4>Usunięto nieaktywe ogłoszenie!</h4></div>');
                    }
                }
            });
        });
    }
});
